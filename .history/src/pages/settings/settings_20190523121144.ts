import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { Settings } from '../../providers';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { User, TypeStatut } from '../../models/user';
import { Camera } from '@ionic-native/camera';
import { ImageServiceProvider } from '../../providers/image-service/image-service';

/**
 * The Settings page is a simple form that syncs with a Settings provider
 * to enable the user to customize settings for the app.
 *
 */
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean;

  item: any;

  form: FormGroup;
  account:User=new User();
  pageTitle: string;
  courtier:boolean=false;
  successMessage:string;
  constructor(public navCtrl: NavController,
    public settings: Settings,
    public formBuilder: FormBuilder,
    public navParams: NavParams,public toastCtrl: ToastController,
    public translateService: TranslateService,public userService:UserServiceProvider,
    private camera:Camera,private imageService:ImageServiceProvider) {
      this.form = formBuilder.group({
        profilePic: [''],
        name: ['', Validators.required],
        about: ['']
      });
  
      // Watch the form for changes, and
      this.form.valueChanges.subscribe((v) => {
        this.isReadyToSave = this.form.valid;
      });
      this.translateService.get('SAVE_SUCCESS').subscribe((value) => {
        this.successMessage = value;
      })
    }
  
    ionViewDidLoad() {
  
    }
  
    getPicture() {
      if (Camera['installed']()) {
        this.camera.getPicture({
          destinationType: this.camera.DestinationType.DATA_URL,
          targetWidth: 96,
          targetHeight: 96
        }).then((data) => {
          this.form.patchValue({ 'profilePic': 'data:image/jpg;base64,' + data });
        }, (err) => {
          alert('Unable to take photo');
        })
      } else {
        this.fileInput.nativeElement.click();
      }
    }
    processWebImage(event) {
      let reader = new FileReader();
      reader.onload = (readerEvent) => {
  
        let imageData = (readerEvent.target as any).result;
        this.form.patchValue({ 'profilePic': imageData });
      };
  
      reader.readAsDataURL(event.target.files[0]);
    }
  
    getProfileImageStyle() {
      return 'url(' + this.form.controls['profilePic'].value + ')'
    }
  ionViewWillEnter(){
      this.userService.getSession();
      this.navParams.data.redirection();
      this.account=this.userService.user_session;
      this.account.email=this.userService.user_session.email;
      this.account.statut=this.userService.user_session.statut;
      this.account.name=this.userService.user_session.name;
      this.account.password=this.userService.user_session.password;
      if(this.account.statut==TypeStatut.COURTIER)
      this.courtier=true;
      this.form.patchValue({ 'profilePic': this.imageService.getImage(this.account.id) });
}
  updateStatut(){
    if(this.courtier)
    this.account.statut=TypeStatut.COURTIER;
    else
    this.account.statut=TypeStatut.CLIENT;
    this.navParams.data.statut=this.account.statut;
    this.userService.updateUser(this.account);
  }
  save(){
    this.imageService.setImage(this.account.id,this.form.controls.profilePic.value);
    this.updateStatut();
    let toast = this.toastCtrl.create({
      message: this.successMessage,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  deconnexion(){
    this.userService.deconnexion();
    this.navParams.data.redirection();
  }
  supprimer(){
    this.userService.deleteUser(this.account);
    this.userService.deconnexion();
    this.navParams.data.redirection();
  }

}
