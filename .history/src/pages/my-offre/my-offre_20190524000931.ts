import { Component } from "@angular/core";
import { IonicPage, ModalController, NavController, NavParams } from "ionic-angular";

import { Item } from "../../models/item";
import { Items } from "../../providers";
import { HouseServiceProvider } from "../../providers/house-service/house-service";
import { House } from "../../models/house";
import { ImageServiceProvider } from "../../providers/image-service/image-service";
import { UserServiceProvider } from "../../providers/user-service/user-service";

@IonicPage()
@Component({
  selector: "page-my-offre",
  templateUrl: "my-offre.html"
})
export class MyOffrePage {
  currentItems: Item[];
  myOffre: House[] = [];

  constructor(
    public navCtrl: NavController,
    public items: Items,
    public modalCtrl: ModalController,
    private houseService: HouseServiceProvider,
    public imageService:ImageServiceProvider,
    private userService:UserServiceProvider,
    public navParams: NavParams
  ) {
    this.currentItems = this.items.query();
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {}

  ionViewWillEnter() {
    if(this.userService.getSession())
    this.myOffre = this.houseService.getMyHouses(this.userService.user_session);
    else this.myOffre=[];
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    if(!this.userService.getSession())
    this.navParams.data.redirection();
    else{
    let addModal = this.modalCtrl.create("OffreCreatePage");
    addModal.onDidDismiss(item => {this.ionViewWillEnter();});
    addModal.present();}
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.houseService.deleteHouse(item);
    this.ionViewWillEnter();
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push("OffreDetailPage", {
      item: item
    });
  }
}
