import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { OffreCreateImagesPage } from './offre-create-images';
import { HouseServiceProvider } from '../../providers/house-service/house-service';

@NgModule({
  declarations: [
    OffreCreateImagesPage,
  ],
  imports: [
    IonicPageModule.forChild(OffreCreateImagesPage),
    TranslateModule.forChild()
  ],
  exports: [
    OffreCreateImagesPage
  ],providers:[HouseServiceProvider]
})
export class OffreCreateImagesPageModule { }
