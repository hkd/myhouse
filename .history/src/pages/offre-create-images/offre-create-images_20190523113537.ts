import { Component, ViewChild, Input } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Camera } from "@ionic-native/camera";
import {
  IonicPage,
  NavController,
  ViewController,
  ToastController,
  DateTime
} from "ionic-angular";
import { House } from "../../models/house";
import { HouseServiceProvider } from "../../providers/house-service/house-service";
import { UserServiceProvider } from "../../providers/user-service/user-service";
import { ImageServiceProvider } from "../../providers/image-service/image-service";

@IonicPage()
@Component({
  selector: "page-offre-create-images",
  templateUrl: "offre-create-images.html"
})
export class OffreCreateImagesPage {
  @ViewChild("fileInput") fileInput;
  @Input("house") house: House = new House();
  isReadyToSave: boolean;

  form: FormGroup;

  constructor(
    public navCtrl: NavController,
    public imageService: ImageServiceProvider,
    private userService: UserServiceProvider,
    public viewCtrl: ViewController,
    private toastCtrl: ToastController,
    formBuilder: FormBuilder,
    public camera: Camera,
    private houseService: HouseServiceProvider
  ) {
    this.form = formBuilder.group({
      profilePic: ["", Validators.required]
    });
    // Watch the form for changes, and
    this.form.valueChanges.subscribe(v => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ionViewDidLoad() {}

  getPicture() {
    if (Camera["installed"]()) {
      this.camera
        .getPicture({
          destinationType: this.camera.DestinationType.DATA_URL,
          targetWidth: 96,
          targetHeight: 96
        })
        .then(
          data => {
            this.form.patchValue({
              profilePic: "data:image/jpg;base64," + data
            });
          },
          err => {
            alert("Unable to take photo");
          }
        );
    } else {
      this.fileInput.nativeElement.click();
    }
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = readerEvent => {
      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({ profilePic: imageData });
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return "url(" + this.form.controls["profilePic"].value + ")";
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  delete(imageId) {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the house, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) {
      return;
    }
    let imageName: string = "" + new Date().getTime();
    this.imageService.setImage(imageName, this.form.controls.profilePic.value);
    this.house = this.houseService.addImage(this.house, imageName);
    let toast = this.toastCtrl.create({
      message: "Enregistrer avec success",
      duration: 3000,
      position: "top"
    });
    toast.present();
  }
}
