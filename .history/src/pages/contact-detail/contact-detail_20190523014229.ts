import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

import { ImageServiceProvider } from "../../providers/image-service/image-service";
import { House } from "../../models/house";
import { User } from "../../models/user";

@IonicPage()
@Component({
  selector: "page-contact-detail",
  templateUrl: "contact-detail.html"
})
export class ContactDetailPage {
  item: User = new User();

  constructor(
    public navCtrl: NavController,
    navParams: NavParams,
    public imageService: ImageServiceProvider
  ) {
    this.item = navParams.get("item");
  }
}
