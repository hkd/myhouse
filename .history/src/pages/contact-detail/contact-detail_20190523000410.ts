import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ImageServiceProvider } from '../../providers/image-service/image-service';
import { House } from '../../models/house';

@IonicPage()
@Component({
  selector: 'page-offre-detail',
  templateUrl: 'offre-detail.html'
})
export class OffreDetailPage {
  item: House;

  constructor(public navCtrl: NavController, navParams: NavParams,public imageService:ImageServiceProvider) {
    this.item = navParams.get('item');
  }

}
