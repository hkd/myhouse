import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";

import { ImageServiceProvider } from "../../providers/image-service/image-service";
import { House } from "../../models/house";
import { User } from "../../models/user";
import { UserServiceProvider } from "../../providers/user-service/user-service";

@IonicPage()
@Component({
  selector: "page-contact-detail",
  templateUrl: "contact-detail.html"
})
export class ContactDetailPage {
  item: User = new User();

  constructor(
    public navCtrl: NavController,
    navParams: NavParams,
    public imageService: ImageServiceProvider,private userService:UserServiceProvider
  ) {
    this.item = navParams.get("item");
    this.item = this.userService.getUser(this.item.id);
  }
}
