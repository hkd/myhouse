import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { OffreDetailPage } from './offre-detail';
import { ImageServiceProvider } from '../../providers/image-service/image-service';
import { OffreCreatePageModule } from '../offre-create/offre-create.module';
import { OffreDetailImageComponent } from './offre-detail-image/offre-detail-image';

@NgModule({
  declarations: [
    OffreDetailPage,OffreDetailImageComponent
  ],
  imports: [
    IonicPageModule.forChild(OffreDetailPage),
    IonicPageModule.forChild(OffreDetailImageComponent),
    TranslateModule.forChild(),OffreCreatePageModule
  ],
  exports: [
    OffreDetailPage
  ],
  providers:[ImageServiceProvider]
})
export class OffreDetailPageModule { }
