import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { OffreDetailPage } from './offre-detail';
import { ImageServiceProvider } from '../../providers/image-service/image-service';

@NgModule({
  declarations: [
    OffreDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(OffreDetailPage),
    TranslateModule.forChild()
  ],
  exports: [
    OffreDetailPage
  ],
  providers:[ImageServiceProvider]
})
export class OffreDetailPageModule { }
