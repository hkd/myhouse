import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { OffreDetailPage } from './offre-detail';
import { ImageServiceProvider } from '../../providers/image-service/image-service';
import { OffreCreatePageModule } from '../offre-create/offre-create.module';
import { OffreCreateImagesPageModule } from '../offre-create-images/offre-create-images.module';

@NgModule({
  declarations: [
    OffreDetailPage
  ],
  imports: [
    IonicPageModule.forChild(OffreDetailPage),
    TranslateModule.forChild(),OffreCreateImagesPageModule
  ],
  exports: [
    OffreDetailPage
  ],
  providers:[ImageServiceProvider]
})
export class OffreDetailPageModule { }
