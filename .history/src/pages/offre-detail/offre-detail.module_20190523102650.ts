import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { OffreDetailPage } from './offre-detail';
import { ImageServiceProvider } from '../../providers/image-service/image-service';
import { OffreCreatePageModule } from '../offre-create/offre-create.module';

@NgModule({
  declarations: [
    OffreDetailPage
  ],
  imports: [
    IonicPageModule.forChild(OffreDetailPage),
    TranslateModule.forChild(),OffreCreatePageModule
  ],
  exports: [
    OffreDetailPage
  ],
  providers:[ImageServiceProvider]
})
export class OffreDetailPageModule { }
