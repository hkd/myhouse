// The page the user lands on after opening the app and without a session
// export const FirstRunPage = 'TutorialPage';
export const FirstRunPage = 'TabsPage';
export const TutorialPage = 'TutorialPage';
export const InscriptionPage = 'TutorialPage';

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'TabsPage';

// The initial root pages for our tabs (remove if not using tabs)
// export const Tab2Root = 'ListMasterPage';
export const Tab1Root = 'CardsPage';
export const Tab2Root = 'ListMasterPage';
export const ContactRoot = 'ContactPage';
export const OffreRoot = 'MyOffrePage';
export const Tab4Root = 'SettingsPage';
