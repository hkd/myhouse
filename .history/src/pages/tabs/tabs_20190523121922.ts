import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { IonicPage, NavController } from "ionic-angular";

import {
  Tab1Root,
  Tab4Root,
  InscriptionPage,
  OffreRoot,
  ContactRoot
} from "../";
import { UserServiceProvider } from "../../providers/user-service/user-service";
import { TypeStatut } from "../../models/user";

@IonicPage()
@Component({
  selector: "page-tabs",
  templateUrl: "tabs.html"
})
export class TabsPage {
  homeRoot: any = Tab1Root;
  offreRoot: any = OffreRoot;
  contactRoot: any = ContactRoot;
  settingRoot: any = Tab4Root;

  tab1Title = " ";
  tab2Title = " ";
  tab3Title = " ";
  tab4Title = " ";
  params: any;

  constructor(
    public navCtrl: NavController,
    public translateService: TranslateService,
    public userService: UserServiceProvider
  ) {
    this.redirection();
    translateService
      .get(["HOME", "OFFRE", "CONTACT", "SETTINGS_TITLE"])
      .subscribe(values => {
        this.tab1Title = values["HOME"];
        this.tab2Title = values["OFFRE"];
        this.tab3Title = values["CONTACT"];
        this.tab4Title = values["SETTINGS_TITLE"];
      });
    this.params = this;
  }
  getStatut(titre:string){
    this.userService.getSession();
    if(this.userService.user_session.statut==TypeStatut.COURTIER)return true;
    return false;
  }

  ionViewWillEnter() {
    this.userService.getSession();
    this.params = this;
  }
  redirection() {
    if (!this.userService.getSession()) this.navCtrl.setRoot(InscriptionPage);
  }
}
