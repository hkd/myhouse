import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController } from 'ionic-angular';

import { Tab1Root, Tab2Root, Tab3Root, Tab4Root, MainPage, InscriptionPage, OffreRoot } from '../';
import { UserServiceProvider } from '../../providers/user-service/user-service';
import { TypeStatut } from '../../models/user';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  statut:TypeStatut=TypeStatut.CLIENT;
  homeRoot: any = Tab1Root;
  offreRoot: any = OffreRoot;
  contactRoot: any = ContactRoot;
  settingRoot: any = Tab4Root;

  tab1Title = " ";
  tab2Title = " ";
  tab3Title = " ";
  tab4Title = " ";
  params:any;

  constructor(public navCtrl: NavController, public translateService: TranslateService,public userService:UserServiceProvider) {
    this.redirection();
      this.statut=userService.user_session.statut;
    translateService.get(['HOME', 'OFFRE', 'CONTACT','SETTINGS_TITLE']).subscribe(values => {
      this.tab1Title = values['HOME'];
      this.tab2Title = values['OFFRE'];
      this.tab3Title = values['CONTACT'];
      this.tab4Title = values['SETTINGS_TITLE'];
    });
    this.params=this;
  }
  ionViewWillEnter(){

    this.params=this;
  }
  redirection(){
    if(!this.userService.getSession())
    this.navCtrl.setRoot(InscriptionPage);
  }
}
