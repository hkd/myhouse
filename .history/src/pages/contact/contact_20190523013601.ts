import { Component } from "@angular/core";
import { IonicPage, ModalController, NavController } from "ionic-angular";
import { UserServiceProvider } from "../../providers/user-service/user-service";
import { User } from "../../models/user";
import { ImageServiceProvider } from "../../providers/image-service/image-service";

@IonicPage()
@Component({
  selector: "page-contact",
  templateUrl: "contact.html"
})
export class ContactPage {
  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public userService: UserServiceProvider,public imageService:ImageServiceProvider
  ) {}

  ionViewDidLoad() {}
  deleteItem(item: User) {
    // this.userService.deleteContact(item.id);
  }
  openItem(item: User) {
    this.navCtrl.push("ContactDetailPage", {
      item: item
    });
  }
}
