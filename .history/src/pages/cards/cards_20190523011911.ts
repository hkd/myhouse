import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { House } from "../../models/house";
import { UserServiceProvider } from "../../providers/user-service/user-service";
import { HouseServiceProvider } from "../../providers/house-service/house-service";
import { ImageServiceProvider } from "../../providers/image-service/image-service";

@IonicPage()
@Component({
  selector: "page-cards",
  templateUrl: "cards.html"
})
export class CardsPage {
  cardItems: House[];

  constructor(
    public navCtrl: NavController,
    private houseService: HouseServiceProvider,
    public imageService: ImageServiceProvider,private userService:UserServiceProvider
  ) {
    this.cardItems = this.houseService.getHouses();
  }
  ionViewWillEnter() {
    this.cardItems = this.houseService.getHouses();
  }

  public abonnement(item: House) {
    if (item.user == undefined) return false;
    if (item.user.id == undefined) return false;
    this.userService.getSession();
    let user=this.userService.getUser(this.userService.user_session.id);
    if(user.id!=item.user.id){
      if(user.abonne==undefined)user.abonne=[];
      let index=user.abonne.findIndex(data=>data==item.user.id);
      if(index==-1)user.abonne.push(item.user.id);
      else user.abonne.splice(index,1);
      this.userService.updateUser(user);
    }
    // this.userService.addContact(item.user.id);
  }
  public isAbonne(item: House): boolean {
    if (item.user == undefined) return false;
    return true; //this.userService.existe(item.user.id);
  }

  openItem(item: House) {
    this.navCtrl.push("OffreDetailPage", {
      item: item
    });
  }
}
