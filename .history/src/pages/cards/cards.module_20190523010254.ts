import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { CardsPage } from './cards';
import { HouseServiceProvider } from '../../providers/house-service/house-service';
import { ImageServiceProvider } from '../../providers/image-service/image-service';
import { CallNumber } from '@ionic-native/call-number/ngx';

@NgModule({
  declarations: [
    CardsPage,
  ],
  imports: [
    IonicPageModule.forChild(CardsPage),
    TranslateModule.forChild()
  ],
  exports: [
    CardsPage
  ],
  providers:[HouseServiceProvider,ImageServiceProvider,CallNumber]
})
export class CardsPageModule { }
