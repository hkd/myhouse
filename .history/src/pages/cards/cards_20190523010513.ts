import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { House } from "../../models/house";
import { UserServiceProvider } from "../../providers/user-service/user-service";
import { HouseServiceProvider } from "../../providers/house-service/house-service";
import { ImageServiceProvider } from "../../providers/image-service/image-service";
import { CallNumber } from "@ionic-native/call-number/ngx";

@IonicPage()
@Component({
  selector: "page-cards",
  templateUrl: "cards.html"
})
export class CardsPage {
  cardItems: House[];

  constructor(
    public navCtrl: NavController,
    private userService: UserServiceProvider,
    private houseService: HouseServiceProvider,
    public imageService: ImageServiceProvider,
     private callNumber: CallNumber
  ) {
    this.cardItems = this.houseService.getHouses();
  }
  ionViewWillEnter() {
    this.cardItems = this.houseService.getHouses();
  }

  public abonnement(item: House) {
    if (item.user == undefined) return false;
    if (item.user.id == undefined) return false;
    // this.userService.addContact(item.user.id);
  }
  public isAbonne(item: House): boolean {
    if (item.user == undefined) return false;
    return true; //this.userService.existe(item.user.id);
  }
  contacter(item: House) {
     this.callNumber.callNumber(item.user.numero,true);
    // if(this.callNumber.isCallSupported)
    // this.callNumber
    //   .callNumber(item.user.numero, true)
    //   .then(res => console.log("Appel en cours"))
    //   .catch(err => console.log(err));
  }

  openItem(item: House) {
    this.navCtrl.push("OffreDetailPage", {
      item: item
    });
  }
}
