import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { CardsPage } from './cards';
import { HouseServiceProvider } from '../../providers/house-service/house-service';
import { ImageServiceProvider } from '../../providers/image-service/image-service';

@NgModule({
  declarations: [
    CardsPage,
  ],
  imports: [
    IonicPageModule.forChild(CardsPage),
    TranslateModule.forChild()
  ],
  exports: [
    CardsPage
  ],
  providers:[HouseServiceProvider,ImageServiceProvider]
})
export class CardsPageModule { }
