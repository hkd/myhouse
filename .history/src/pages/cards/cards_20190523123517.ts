import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { House } from "../../models/house";
import { UserServiceProvider } from "../../providers/user-service/user-service";
import { HouseServiceProvider } from "../../providers/house-service/house-service";
import { ImageServiceProvider } from "../../providers/image-service/image-service";

@IonicPage()
@Component({
  selector: "page-cards",
  templateUrl: "cards.html"
})
export class CardsPage {
  cardItems: House[];
  cardItemsDataBase: House[];

  constructor(
    public navCtrl: NavController,
    private houseService: HouseServiceProvider,
    public imageService: ImageServiceProvider,public userService:UserServiceProvider
  ) {
    this.cardItems = this.houseService.getHouses();
    this.cardItemsDataBase=this.cardItems;
  }
  ionViewWillEnter() {
    this.cardItems = this.houseService.getHouses();
  }

  public abonnement(item: House) {
    if (item.user == undefined) return false;
    if (item.user.id == undefined) return false;
  }
  public contacter(itemD:House){
let item=itemD.user;
    this.navCtrl.push("ContactDetailPage", {
      item: item
    });
  }
  public isAbonne(item: House): boolean {
    if (item.user == undefined) return false;
    return true; //this.userService.existe(item.user.id);
  }

  openItem(item: House) {
    this.navCtrl.push("OffreDetailPage", {
      item: item
    });
  }
  filtre(ev:any){
    var val = ev.target.value;
    console.log(val);
    this.cardItems=this.cardItemsDataBase.filter(data=>data.content.includes(val)||data.price.toString().includes(val))
  }
}
