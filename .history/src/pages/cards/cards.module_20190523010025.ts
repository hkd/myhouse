import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { CardsPage } from './cards';
import { HouseServiceProvider } from '../../providers/house-service/house-service';
import { ImageServiceProvider } from '../../providers/image-service/image-service';
import { CallNumberOriginal } from '@ionic-native/call-number';

@NgModule({
  declarations: [
    CardsPage,
  ],
  imports: [
    IonicPageModule.forChild(CardsPage),
    TranslateModule.forChild()
  ],
  exports: [
    CardsPage
  ],
  providers:[HouseServiceProvider,ImageServiceProvider,CallNumberOriginal]
})
export class CardsPageModule { }
