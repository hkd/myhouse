import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { House } from "../../models/House";
import { User } from "../../models/user";
@Injectable()
export class HouseServiceProvider {

  COLLECTION="HOUSES";

  constructor(public http: HttpClient,
    // public navCtrl: NavController
    ) {
    // this.init_data();
  }

  public saveHouse(liste:House[]){
    localStorage.removeItem(this.COLLECTION);
    localStorage.setItem(this.COLLECTION,JSON.stringify(liste));
  }
  public addHouse(house:House){
    if(!house.id)
    house.id=(new Date()).getTime();
    let liste:House[]=this.getHouses();
    liste.push(house);
    this.saveHouse(liste);
  }
  public deleteHouse(house:House){
    let liste:House[]=this.getHouses();
    liste.splice(liste.findIndex(HouseDate=>HouseDate.id==house.id),1);
    this.saveHouse(liste);
  }
  public updateHouse(house:House){
    this.deleteHouse(house);
    this.addHouse(house);
  }
  public getHouse(id:number):House{
    return this.getHouses().find(HouseData=>HouseData.id==id);
  }
  public getHouses():House[]{
    if(localStorage.getItem(this.COLLECTION)!=null)
      return JSON.parse(localStorage.getItem(this.COLLECTION));
    return [];
  }
  public getMyHouses(user:User):House[]{
    if(localStorage.getItem(this.COLLECTION)==null)return [];

      let liste:House[]=JSON.parse(localStorage.getItem(this.COLLECTION));
      let myListe:House[]=[];
      liste.forEach(house=>{
        if(house.user.id==user.id)myListe.push(house);
      })
    return myListe;
  }
  public addImage(house:House,imageName:string){
    house=this.getHouse(house.id);
    console.log("Pas d'erreur ici ",house);
    if(house.images==undefined||house.images==null)house.images=[];
    house.images.push(imageName);
    this.updateHouse(house);
    return house;
  }
  public deleteImage(house:House,imageName:string){
    house=this.getHouse(house.id);
    if(house.images==undefined)house.images=[];
    house.images.splice(house.images.findIndex(data=>data.includes(imageName)));
    this.updateHouse(house);
  }
}
