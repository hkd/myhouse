import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ImageServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ImageServiceProvider {

  constructor() {
  }
  
  setImage(name,img){
    if(localStorage.getItem(name)!=null)
    localStorage.removeItem(name);
    localStorage.setItem(name,img);
  }
  getImage(name){
    console.log("====================="+name)
    if(name==undefined)return "";
    return localStorage.getItem(name);
  }
  getImageUrl(name){
    return 'url(' + localStorage.getItem(name) + ')';
  }
  removeImage(name){
    localStorage.removeItem(name);
  }

}
