import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the InitDataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InitDataServiceProvider {

  constructor(public http: Http) {
    console.log('Hello InitDataServiceProvider Provider');
  }
  getImage(chemin:string){
    
    this.http.request(chemin)
          .map(res => res.text())
          .subscribe(text => {
            return text;
          });
  }

}
