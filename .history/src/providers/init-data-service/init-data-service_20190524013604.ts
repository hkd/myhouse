import { Http } from "@angular/http";
import { Injectable } from "@angular/core";
import { House } from "../../models/house";
import { User, TypeStatut } from "../../models/user";
import { UserServiceProvider } from "../user-service/user-service";
import { ImageServiceProvider } from "../image-service/image-service";
import { HouseServiceProvider } from "../house-service/house-service";


/*
  Generated class for the InitDataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InitDataServiceProvider {
  constructor(
    public http: Http,
    public userService: UserServiceProvider,
    public imageService: ImageServiceProvider,
    public houseService:HouseServiceProvider
  ) {
    
  }
  getImage(chemin: string) {
    this.http
      .request(chemin)
      .map(res => res.text())
      .subscribe(text => {
        return text;
      });
  }
  userD: User = {
    id: 12345,
    name: "DAO Hamadou",
    email: "admin@gmail.com",
    numero: "783741614",
    password: "admin",
    statut: TypeStatut.CLIENT,
    abonne: []
  };
  houses: House[] = [
    {
      id: 100,
      content:
        "Haec ubi latius fama vulgasset missaeque relationes adsiduae Gallum" +
        "Caesarem permovissent, quoniam magister equitum longius ea tempestate distinebatur",
      date: "" + new Date(),
      images: [],
      price: 30000,
      user: this.userD
    },
    {
      id: 101,
      content:
        "Haec ubi latius fama vulgasset missaeque relationes adsiduae Gallum" +
        "Caesarem permovissent, quoniam magister equitum longius ea tempestate distinebatur",
      date: "" + new Date(),
      images: [],
      price: 10000,
      user: this.userD
    },
    {
      id: 102,
      content:
        "Haec ubi latius fama vulgasset missaeque relationes adsiduae Gallum" +
        "Caesarem permovissent, quoniam magister equitum longius ea tempestate distinebatur",
      date: "" + new Date(),
      images: [],
      price: 20000,
      user: this.userD
    }
  ];
}
