import { Http } from "@angular/http";
import { Injectable } from "@angular/core";
import { House } from "../../models/house";
import { User, TypeStatut } from "../../models/user";

/*
  Generated class for the InitDataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InitDataServiceProvider {
  constructor(public http: Http) {
    console.log("Hello InitDataServiceProvider Provider");
  }
  getImage(chemin: string) {
    this.http
      .request(chemin)
      .map(res => res.text())
      .subscribe(text => {
        return text;
      });
  }
  userD: User = {
    id: 12345,
    name: "DAO Hamadou",
    email: "admin@gmail.com",
    numero: "783741614",
    password: "admin",
    statut: TypeStatut.CLIENT,
    abonne: []
  };
  houses: House[] = [
    {
      id: 100,
      content: "",
      date: "" + new Date(),
      images: [],
      price: 100,
      user: this.userD
    },
    {
      id: 101,
      content: "",
      date: "" + new Date(),
      images: [],
      price: 100,
      user: this.userD
    },
    {
      id: 102,
      content: "",
      date: "" + new Date(),
      images: [],
      price: 100,
      user: this.userD
    },
  ];
}
