import { Http } from "@angular/http";
import { Injectable } from "@angular/core";
import { House } from "../../models/house";
import { User, TypeStatut } from "../../models/user";
import { UserServiceProvider } from "../user-service/user-service";
import { ImageServiceProvider } from "../image-service/image-service";
import { HouseServiceProvider } from "../house-service/house-service";

import { FileOpener } from '@ionic-native/file-opener/ngx';
/*
  Generated class for the InitDataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InitDataServiceProvider {
  DIR:string="assets/img/";
  constructor(
    public http: Http,
    public userService: UserServiceProvider,
    public imageService: ImageServiceProvider,
    public houseService:HouseServiceProvider
  ) {
    
  }
  initData(){
    for(let user of this.userD){
    this.imageService.setImage(user.id+5000,this.DIR+user.id+".png");
    user.id=user.id+5000;
    this.userService.addUser(user);
  }
    for(let house of this.houses){
        this.imageService.setImage(house.id+20,this.DIR+house.id+".jpg");
        house.id=house.id+20;
        for(let im of house.images){
          this.imageService.setImage(im,this.DIR+im+".jpg");
        }
        this.houseService.addHouse(house);
    }
  }
  getImage(chemin: string) {
    this.http
      .request(chemin)
      .map(res => res.arrayBuffer())
      .subscribe(text => {
        return text;
      });
  }
  userD: User[] = [{
    id: 1,
    name: "DAO Hamadou",
    email: "admin@gmail.com",
    numero: "783741614",
    password: "admin",
    statut: TypeStatut.COURTIER,
    abonne: []
  },{
    id: 2,
    name: "Awa THIAM",
    email: "awa@gmail.com",
    numero: "000000000",
    password: "admin",
    statut: TypeStatut.COURTIER,
    abonne: []
  },
];
  houses: House[] = [
    {
      id: 1,
      content:
        "Haec ubi latius fama vulgasset missaeque relationes adsiduae Gallum" +
        "Caesarem permovissent, quoniam magister equitum longius ea tempestate distinebatur",
      date: "" + new Date(),
      images: [12+"",13+"",14+"",15+""],
      price: 30000,
      user: this.userD[0]
    },
    {
      id: 2,
      content:
        "Haec ubi latius fama vulgasset missaeque relationes adsiduae Gallum" +
        "Caesarem permovissent, quoniam magister equitum longius ea tempestate distinebatur",
      date: "" + new Date(),
      images: [22+"",23+"",24+"",25+""],
      price: 10000,
      user: this.userD[1]
    },
    {
      id: 3,
      content:
        "Haec ubi latius fama vulgasset missaeque relationes adsiduae Gallum" +
        "Caesarem permovissent, quoniam magister equitum longius ea tempestate distinebatur",
      date: "" + new Date(),
      images: [],
      price: 20000,
      user: this.userD[1]
    },
    {
      id: 4,
      content:
        "Chambre salon douche interne medina" +
        "Gueule tappée",
      date: "" + new Date(),
      images: [],
      price: 55000,
      user: this.userD[0]
    },
    {
      id: 5,
      content:
        "3 Chambres salon douche interne medina" +
        "Sacrée coeur",
      date: "" + new Date(),
      images: [],
      price: 75000,
      user: this.userD[0]
    }
  ];
}
