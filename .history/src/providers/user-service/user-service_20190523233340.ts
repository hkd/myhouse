import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { User } from "../../models/user";

@Injectable()
export class UserServiceProvider {
  public user_session: User = new User();

  listeUser: User[];
  COLLECTION = "USERS";

  constructor(public http: HttpClient) // public navCtrl: NavController
  {
    // this.init_data();
  }
  public getSession(): boolean {
    if (localStorage.getItem("user") != null) {
      this.user_session = JSON.parse(localStorage.getItem("user"));
      this.user_session = this.getUser(this.user_session.id);
      return true;
    }
    return false;
  }
  public deconnexion() {
    localStorage.removeItem("user");
  }
  public login(user: User) {
    const userIdentify = this.getUsers().find(
      userData =>
        userData.email.includes(user.email) &&
        userData.password.includes(user.password)
    );
    if (!userIdentify) {
      localStorage.removeItem("user");
      return false;
    }
    this.user_session = userIdentify;
    localStorage.setItem("user", JSON.stringify(userIdentify));
    return true;
  }

  public signup(user: User) {
    const userIdentify = this.getUsers().find(userData =>
      userData.email.includes(user.email)
    );
    if (userIdentify != null) return false;
    this.addUser(user);
    this.login(user);
    return true;
  }
  public saveUser(liste: User[]) {
    localStorage.removeItem(this.COLLECTION);
    localStorage.setItem(this.COLLECTION, JSON.stringify(liste));
  }
  public addUser(user: User) {
    if (!user.id) user.id = new Date().getTime();
    let liste: User[] = this.getUsers();
    liste.push(user);
    this.saveUser(liste);
  }
  public deleteUser(user: User) {
    let liste: User[] = this.getUsers();
    liste.splice(liste.findIndex(userDate => userDate.id == user.id), 1);
    this.saveUser(liste);
  }
  public updateUser(user: User) {
    this.deleteUser(user);
    this.addUser(user);
    localStorage.setItem("user", JSON.stringify(user));
  }
  public getUser(id: number): User {
    return this.getUsers().find(userData => userData.id == id);
  }
  public isAbonne(id) {
    if(!this.getSession())return false;
    let datas =[];// this.getUser(this.user_session.id).abonne;
    if (datas == undefined) return false;
    for (let data of datas) if (data == id) return true;
    return false;
  }
  public getUsers(): User[] {
    if (localStorage.getItem(this.COLLECTION) != null)
      return JSON.parse(localStorage.getItem(this.COLLECTION));
    return [];
  }
  public getContacts() {
    this.getSession();
    let users: User[] = [];
    let datas = this.getUser(this.user_session.id).abonne;
    if (datas == undefined) return [];
    for (let data of datas) {
      let user = this.getUser(data);
      if (user != null) users.push(user);
    }
    return users;
  }
  public addOrDeleteContact(itemUser:User){
    this.getSession();
    let user=this.getUser(this.user_session.id);
    if(user.id!=itemUser.id){
      if(user.abonne==undefined)user.abonne=[];
      let index=user.abonne.findIndex(data=>data==itemUser.id);
      if(index==-1)user.abonne.push(itemUser.id);
      else user.abonne.splice(index,1);
      this.updateUser(user);
    }
  }
}
