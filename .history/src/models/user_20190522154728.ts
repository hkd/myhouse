export class User{
    id?:number; 
    $key?:string;
    email?: string; 
    password?: string;
    fullname?:string;
    avatar?:string;
    name?:string;
    numero?:string;
    statut?:TypeStatut=TypeStatut.CLIENT;
 }
 export enum TypeStatut{
     CLIENT="CLIENT",COURTIER="COURTIER"
 }