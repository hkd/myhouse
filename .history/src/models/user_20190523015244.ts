export class User{
    id?:number; 
    email?: string; 
    password?: string;
    name?:string;
    numero?:string;
    statut?:TypeStatut=TypeStatut.CLIENT;
    abonne?:number[];
 }
 export enum TypeStatut{
     CLIENT="CLIENT",COURTIER="COURTIER"
 }