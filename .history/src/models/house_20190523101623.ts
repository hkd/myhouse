import { User } from "./user";

export class House{ 
    id?:number;
    date: string; 
    image: string;
    images?:string[];
    content?:string;
    price?:number;
    user:User;
 }