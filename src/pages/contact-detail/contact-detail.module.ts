import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { ContactDetailPage } from './contact-detail';
import { ImageServiceProvider } from '../../providers/image-service/image-service';

@NgModule({
  declarations: [
    ContactDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactDetailPage),
    TranslateModule.forChild()
  ],
  exports: [
    ContactDetailPage
  ],
  providers:[ImageServiceProvider]
})
export class ContactDetailPageModule { }
