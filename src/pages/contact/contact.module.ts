import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactPage } from './contact';
import { ImageServiceProvider } from '../../providers/image-service/image-service';


@NgModule({
  declarations: [
    ContactPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactPage),
    TranslateModule.forChild()
  ],
  exports: [
    ContactPage
  ],
  providers:[ImageServiceProvider]
})
export class ContactModule { }
