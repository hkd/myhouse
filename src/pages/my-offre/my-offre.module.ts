import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { MyOffrePage } from './my-offre';
import { HouseServiceProvider } from '../../providers/house-service/house-service';

@NgModule({
  declarations: [
    MyOffrePage,
  ],
  imports: [
    IonicPageModule.forChild(MyOffrePage),
    TranslateModule.forChild()
  ],
  exports: [
    MyOffrePage
  ],
  providers:[HouseServiceProvider]
})
export class MyOffrePageModule { }
