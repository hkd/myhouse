import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { OffreCreatePage } from './offre-create';
import { HouseServiceProvider } from '../../providers/house-service/house-service';

@NgModule({
  declarations: [
    OffreCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(OffreCreatePage),
    TranslateModule.forChild()
  ],
  exports: [
    OffreCreatePage
  ],providers:[HouseServiceProvider]
})
export class OffreCreatePageModule { }
