import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User, TypeStatut } from '../../models/user';
import { MainPage } from '../';
import { UserServiceProvider } from '../../providers/user-service/user-service';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  
  account:User={
    name: 'Test Human',
    email: 'test@example.com',
    password: 'test',
    statut:TypeStatut.CLIENT
  }

  // Our translated text strings
  private signupErrorString: string;

  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public translateService: TranslateService,public userService:UserServiceProvider) {
      if(this.userService.getSession())
      this.navCtrl.push(MainPage);
    this.translateService.get('SIGNUP_ERROR').subscribe((value) => {
      this.signupErrorString = value;
    })
  }

  doSignup() {
    if(!this.userService.signup(this.account)){
      let toast = this.toastCtrl.create({
        message: this.signupErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
    else this.navCtrl.push(MainPage);
    console.log(this.account.statut);
  }
}
