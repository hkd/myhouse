import { Injectable } from '@angular/core';

@Injectable()
export class ImageServiceProvider {

  constructor() {
  }
  
  setImage(name,img){
    if(localStorage.getItem(name)!=null)
    localStorage.removeItem(name);
    localStorage.setItem(name,img);
  }
  getImage(name:any){
    let na=name+"";
    if(name==undefined)return "";
    return localStorage.getItem(na);
  }
  getImageUrl(name){
    return 'url(' + localStorage.getItem(name) + ')';
  }
  removeImage(name){
    localStorage.removeItem(name);
  }

}
